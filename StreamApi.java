import java.util.Arrays;
public class StreamApi {
    public static void main (String[ ] args){
        int[]arr={2,3,4,5,6};
        int sum=Arrays.stream(arr).
                map(x->x*x).
                map(mapmetod::operation).sum();
        System.out.println(sum);
    }
}
