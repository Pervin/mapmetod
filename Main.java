import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        product product1 = new product("NOKIA", "mobile", 1000.35, 2);
        product product2 = new product("LENAVO", "mobile", 1500.20, 1);
        product product3 = new product("NOKIA", "mobile", 1002.34, 3);
        product product4 = new product("LG", "TV", 1900, 2);
        List<product> Product = List.of(product1, product2, product3, product4);
        List<product> mobileProducts = Product.stream()
                .filter(product -> product.getCategory().equals("mobile"))
                .sorted((p1, p2) -> {
                    int rersult = Double.compare(p2.getTotalPrice(), p1.getTotalPrice());
                    return rersult != 0 ? rersult : p1.getName().compareTo(p2.getName());
                })
                .collect(Collectors.toList());


        mobileProducts.forEach(product -> {
            System.out.println(product.getName() + " " + product.getTotalPrice());
        });
    }}
